package com.cprime.bonfireevents.adapter;

import com.cprime.bonfireevents.domain.Organizer;

public interface OrganizerAdapter {

    Organizer getOrganizer(int i);
}
